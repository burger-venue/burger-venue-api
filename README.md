# burger-venue-api

Burger-venue-api is the application for retrieving burger places in Tartu and finding burger images from these venues.

# Project structure
Application is built using hexagonal architecture and consists of 4 main parts
```
common/            common sharable code, no external frameworks!
adapters/          adapters for implementing outside communication
application/       core business logic
configuration/     spring boot module and all configurations
```

### Development environment setup
* JDK17 and JAVA_HOME set
* IDEA plugins: Lombok
* Enable -> File | Settings | Build, Execution, Deployment | Compiler | Annotation Processors

### Deployment/testing of the application

Application is deployed to heroku:
* To retrieve venues make get request to URL: https://burger-venue.herokuapp.com/burger-venue/api/venues
* The request returns cursor and the list of venues containing name, description and the burger image url of the venue. If no burger images found, null is returned
* To retrieve next venues make request to URL: https://burger-venue.herokuapp.com/burger-venue/api/venues/cursor/{cursor} and use cursor parameter received in previous response
* Additionally check out the front-end application: https://gitlab.com/burger-venue/burger-venue-web
